package cymru.prv.dictionary.gaelic.tenses;


import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;


/**
 * Tests for verb tenses
 *
 * @author Preben Vangberg
 * @since 1.0.1
 */
public class TestGaelicTense {

    @Test
    public void testDefaultsExtractForPresentTense(){
        String result = "default";
        GaelicVerb verb = new GaelicVerb(new JSONObject()
                .put("normalForm", "test")
                .put("present", new JSONObject().put("defaults", result))
        );

        var obj = verb.toJson().getJSONObject("conjugations").getJSONObject("present");
        Assertions.assertEquals(List.of(result), Json.getStringList(obj, "singSecond"));
    }

    @Test
    public void testDefaultsWithOverride(){
        String result = "default";
        GaelicVerb verb = new GaelicVerb(new JSONObject()
                .put("normalForm", "test")
                .put("present", new JSONObject()
                        .put("defaults", result)
                        .put("singThird", "third"))
        );

        var obj = verb.toJson().getJSONObject("conjugations").getJSONObject("present");
        Assertions.assertEquals(List.of(result), Json.getStringList(obj, "singFirst"));
        Assertions.assertEquals(List.of("third"), Json.getStringList(obj, "singThird"));
    }

    @Test
    public void testMultipleDefaults(){
        List<String> result = List.of("default1", "default2");
        GaelicVerb verb = new GaelicVerb(new JSONObject()
                .put("normalForm", "test")
                .put("present", new JSONObject()
                        .put("defaults", result)
                        .put("singThird", "third"))
        );

        var obj = verb.toJson().getJSONObject("conjugations").getJSONObject("present");
        Assertions.assertEquals(result, Json.getStringList(obj, "plurFirst"));
    }


}
