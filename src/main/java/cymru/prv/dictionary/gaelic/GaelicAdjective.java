package cymru.prv.dictionary.gaelic;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents a Scottish Gaelic adjective
 *
 * @author Zander Urq.
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class GaelicAdjective extends Word {

	private static final String COMPARATIVE = "comparative";
	private static final String SUPERLATIVE = "superlative";

	private final String stem;
	private final List<String> comparative;
	private final List<String> superlative;
	
	public GaelicAdjective(JSONObject obj) {
        super(obj, WordType.adjective);
        stem = obj.optString("stem", getNormalForm());
		comparative = Json.getStringListOrDefault(obj, COMPARATIVE, this::getComparative);
		superlative = Json.getStringListOrDefault(obj, SUPERLATIVE, this::getSuperlative);
	}

	@Override
	protected JSONObject getInflections() {
		JSONObject obj = new JSONObject();
		obj.put(COMPARATIVE, comparative);
		obj.put(SUPERLATIVE, superlative);
		return obj;
	}

	private List<String> getComparative() {
		return Collections.singletonList("nas " + attenuate(stem) + "e");
	}

	private List<String> getSuperlative() {
		return Collections.singletonList("as " + attenuate(stem) + "e");
	}

	@Override
	public List<String> getVersions() {
		var list = super.getVersions();
		list.addAll(comparative);
		list.addAll(superlative);
		return list;
	}

	private static String attenuate(String stem) {
		if(stem.endsWith("e")) {
			stem = stem.substring(0, stem.length() - 1);
		}
		
		int lastVowel = -1;

		for (int i = 0; i < stem.length(); i++) {
			if (stem.charAt(i) == 'a' || stem.charAt(i) == 'e' || stem.charAt(i) == 'i' || stem.charAt(i) == 'o'
					|| stem.charAt(i) == 'u' || stem.charAt(i) == 'à' || stem.charAt(i) == 'è' || stem.charAt(i) == 'ì'
					|| stem.charAt(i) == 'ò' || stem.charAt(i) == 'ù') {
				lastVowel = i;
			}
		}
		
		String att = "";

		if (stem.charAt(lastVowel) == 'e' || stem.charAt(lastVowel) == 'i' || stem.charAt(lastVowel) == 'è'
				|| stem.charAt(lastVowel) == 'ì') {
			att = stem;
		} else {
			if (lastVowel != 0 && stem.charAt(lastVowel - 1) == 'e' && stem.charAt(lastVowel) == 'a') {
				for (int i = 0; i < stem.length(); i++) {
					if (i == lastVowel) {
						// do nothing
					} else if (i == lastVowel - 1) {
						att += "i";
					} else {
						att += stem.charAt(i);
					}
				}
			} else {
				for (int i = 0; i < stem.length(); i++) {
					att += stem.charAt(i);
					if (i == lastVowel) {
						att += "i";
					}
				}
			}
		}

		return att;
	}
}
