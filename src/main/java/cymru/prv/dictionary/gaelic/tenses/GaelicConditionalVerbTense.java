package cymru.prv.dictionary.gaelic.tenses;

import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicConditionalVerbTense extends GaelicVerbTense {

	public GaelicConditionalVerbTense(GaelicVerb verb, JSONObject obj) {
		super(verb, obj);
	}

	@Override
	protected List<String> getDefaultSingularFirst() {
		return Collections.singletonList(applyBroadOrSlender("ainn", "inn"));
	}

	@Override
	protected List<String> getDefaultSingularSecond() {
		return Collections.singletonList(applyBroadOrSlender("adh tu", "eadh tu"));
	}

	@Override
	protected List<String> getDefaultSingularThird() {
		return Collections.singletonList(applyBroadOrSlender("adh e/i", "eadh e/i"));
	}

	@Override
	protected List<String> getDefaultPluralFirst() {
		return Collections.singletonList(applyBroadOrSlender("amaid", "eamaid"));
	}

	@Override
	protected List<String> getDefaultPluralSecond() {
		return Collections.singletonList(applyBroadOrSlender("adh sibh", "eadh sibh"));
	}

	@Override
	protected List<String> getDefaultPluralThird() {
		return Collections.singletonList(applyBroadOrSlender("adh iad", "eadh iad"));
	}

	@Override
	protected List<String> getDefaultImpersonal() {
		return Collections.emptyList();
	}

}
