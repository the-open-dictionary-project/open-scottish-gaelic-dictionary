package cymru.prv.dictionary.gaelic.tenses;

import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicImperativeVerbTense extends GaelicVerbTense {

	public GaelicImperativeVerbTense(GaelicVerb verb, JSONObject obj) {
		super(verb, obj);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<String> getDefaultSingularFirst() {
		return Collections.singletonList(applyBroadOrSlender("am", "eam"));
	}

	@Override
	protected List<String> getDefaultSingularSecond() {
		return getDefaultImpersonal();
	}

	@Override
	protected List<String> getDefaultSingularThird() {
		return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
	}

	@Override
	protected List<String> getDefaultPluralFirst() {
		return Collections.singletonList(applyBroadOrSlender("amaid", "eamaid"));
	}

	@Override
	protected List<String> getDefaultPluralSecond() {
		return Collections.singletonList(applyBroadOrSlender("aibh", "ibh"));
	}

	@Override
	protected List<String> getDefaultPluralThird() {
		return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
	}

	@Override
	protected List<String> getDefaultImpersonal() {
		return Collections.singletonList(stem);
	}

}
