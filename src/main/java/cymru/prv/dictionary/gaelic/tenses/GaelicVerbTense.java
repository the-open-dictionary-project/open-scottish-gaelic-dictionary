package cymru.prv.dictionary.gaelic.tenses;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.gaelic.GaelicLenition;
import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;

import java.util.List;
import java.util.function.Supplier;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public abstract class GaelicVerbTense extends Conjugation {

    private static final String ANALYTIC = "analytic";
    private static final String DEFAULTS = "defaults";

    protected final String stem;
    private final List<String> analytic;
    private final List<String> defaults;
    protected List<String> defaultAnalytic() {
		return null;
	}

    private List<String>getAnalytic(){
        if(analytic != null)
            return analytic;
        return defaultAnalytic();
    }
    
    public GaelicVerbTense(GaelicVerb verb, JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", verb.getNormalForm());
        if(obj.has(DEFAULTS))
            defaults = Json.getStringList(obj, DEFAULTS);
        else
            defaults = null;
        if(obj.has(ANALYTIC))
            analytic = Json.getStringList(obj, ANALYTIC);
        else
            analytic = null;
    }

    @Override
    public JSONObject toJson() {
        return super.toJson().put("analytic", getAnalytic());
    }

    protected String apply(String suffix) {
        if(stem.endsWith("" + suffix.charAt(0)))
            return stem + suffix.substring(1);
        return stem + suffix;
    }

    protected String applyBroadOrSlender(String broad, String slender) {
        if(GaelicLenition.isBroad(stem))
            return apply(broad);
        else
            return apply(slender);
    }

    protected List<String> getDefaultOrFunction(Supplier<List<String>> otherwise){
        if(defaults == null)
            return otherwise.get();
        return defaults;
    }

    @Override
    protected abstract List<String> getDefaultSingularFirst();

    @Override
    protected abstract List<String> getDefaultSingularSecond();

    @Override
    protected abstract List<String> getDefaultSingularThird();

    @Override
    protected abstract List<String> getDefaultPluralFirst();

    @Override
    protected abstract List<String> getDefaultPluralSecond();

    @Override
    protected abstract List<String> getDefaultPluralThird();

    @Override
    protected abstract List<String> getDefaultImpersonal();
}
