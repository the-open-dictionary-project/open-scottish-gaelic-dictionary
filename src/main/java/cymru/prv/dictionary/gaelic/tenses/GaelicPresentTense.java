package cymru.prv.dictionary.gaelic.tenses;


import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * Represents the present tense in Gaelic
 *
 * @author Preben Vangberg
 * @since 1.0.1
 */
public class GaelicPresentTense extends GaelicVerbTense {

    public GaelicPresentTense(GaelicVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return getDefaultOrFunction(Collections::emptyList);
    }


}
