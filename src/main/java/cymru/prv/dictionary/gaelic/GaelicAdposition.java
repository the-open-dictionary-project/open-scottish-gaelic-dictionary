package cymru.prv.dictionary.gaelic;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Inflection;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.List;

public class GaelicAdposition extends Word {

    private Inflection inflection;

    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @param obj        The data for the preposition
     * @throws IllegalArgumentException if normalForm is empty or null
     */
    public GaelicAdposition(JSONObject obj) {
        super(obj, WordType.adposition);
        if(obj.optBoolean("inflected", false))
            inflection = new Inflection(obj);
    }

    @Override
    protected JSONObject getInflections() {
        if(inflection != null)
            return inflection.toJson();
        return null;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(inflection != null)
            list.addAll(inflection.getVersions());
        return list;
    }
}
