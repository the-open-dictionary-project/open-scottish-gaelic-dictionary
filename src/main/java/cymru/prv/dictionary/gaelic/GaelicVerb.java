package cymru.prv.dictionary.gaelic;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.gaelic.tenses.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;


/**
 * Represents a Scottish Gaelic verb
 *
 * @author Zander Urq.
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class GaelicVerb extends Word {

    public static final String PRESENT = "present";
    public static final String PRETERITE = "preterite";
    public static final String IMPERATIVE = "imperative";
    public static final String FUTURE = "future";
    public static final String CONDITIONAL = "conditional";
    public static final String FUTURE_RELATIVE = "future_relative";

    private final Map<String, Conjugation> tenses = new HashMap<>();

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public GaelicVerb(JSONObject obj) {
        super(obj, WordType.verb);
        addRequiredTense(obj, PRETERITE, GaelicPastVerbTense::new);
        addRequiredTense(obj, FUTURE, GaelicFutureVerbTense::new);
        addRequiredTense(obj, CONDITIONAL, GaelicConditionalVerbTense::new);
        addRequiredTense(obj, IMPERATIVE, GaelicImperativeVerbTense::new);
        addRequiredTense(obj, FUTURE_RELATIVE, GaelicRelativeFutureVerbTense::new);

        //Special tense for bí
        addOptionalTense(obj, PRESENT, GaelicPresentTense::new);
    }

    private static final String  regexVowels = "(aío|aoi|aoú|ia(i|)|ua(i|)|eái|ea(i|)|eo(i|)|uío|uói|iái|iói|iúi|uái|oío|" +
            "ae(i|)|ái|ai|ao|aí|éa|eá|ei|éi|ío|" +
            "uó|oi|iá|ió|iú|io|uí|úi|iu|uá|ói|oí|ui|á|a|e|é|i|í|o|ó|u|ú)";

    @Override
    protected JSONObject getConjugations() {
        JSONObject obj =  new JSONObject();
        for(String key : tenses.keySet())
            obj.put(key, tenses.get(key).toJson());
        return obj;
    }

    private void addRequiredTense(JSONObject obj, String key, BiFunction<GaelicVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
        else
            tenses.put(key, func.apply(this, new JSONObject()));
    }

    private void addOptionalTense(JSONObject obj, String key, BiFunction<GaelicVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        for(Conjugation c : tenses.values())
            list.addAll(c.getVersions());
        return list;
    }
}
